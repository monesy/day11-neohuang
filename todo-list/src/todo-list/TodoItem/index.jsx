import { useDispatch } from "react-redux"
import { deleteTodo, updateDone } from "../TodoSlice"
import './index.css'

function Item({ item }) {
    const dispatch = useDispatch()

    function clickChange() {
        dispatch(updateDone(item))

    }

    return (
        <div>
            <div style={{ border: "1px solid", width: "300px", padding: "5px", margin: "5px", textAlign: "center" }} onClick={clickChange} >
                <span style={{
                    textDecoration: item.done ? 'line-through' : ''
                }}>{item.text}
                </span>

                <span className="button">
                    <input value={"X"} type="Submit" onClick={() => { dispatch(deleteTodo(item.id)) }}></input>
                </span>
            </div>


        </div>
    )
}
export default Item