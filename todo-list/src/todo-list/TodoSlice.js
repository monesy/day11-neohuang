import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
    name: 'todo',
    initialState: {
        todoList: [{
            id: "0",
            text: "wdnmd",
            done: false
        }],
    },
    reducers: {

        generateTodo: (state, action) => {
            const obj = {
                id: action.payload.id,
                text: action.payload.text,
                done: action.payload.done
            }
            state.todoList.push(obj)

        },
        updateDone: (state, action) => {
            const updateStatus = state.todoList.findIndex((todoItem) => todoItem.id == action.payload.id)
            if (updateStatus == action.payload) {
                const tmpTodoItem = state.todoList[updateStatus]
                tmpTodoItem.done = !tmpTodoItem.done
                state.todoList[updateStatus] = tmpTodoItem
            }

        },

        deleteTodo: (state, action) => {
            state.todoList = state.todoList.filter(todoItem => Number(todoItem.id) !== Number(action.payload))
        },

    },
})

// Action creators are generated for each case reducer function
export const { generateTodo, updateDone, deleteTodo } = counterSlice.actions

export default counterSlice.reducer