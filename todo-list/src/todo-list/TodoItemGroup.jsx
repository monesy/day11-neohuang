import { useSelector } from "react-redux"
import TodoItem from "./TodoItem"

const TodoItemGroup = () => {
    const todoList = useSelector(state => state.todo.todoList)
    return (
        <div>
            {
                todoList.map((item, index) => <TodoItem key={index} item={item} />)
            }
        </div>

    )
}
export default TodoItemGroup