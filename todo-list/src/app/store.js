import { configureStore } from '@reduxjs/toolkit'
import todoSlice from '../todo-list/TodoSlice'

export default configureStore({
  reducer: {
    todo: todoSlice,
  },
})