import { useState } from "react"
import { useDispatch } from "react-redux"
import { generateTodo } from "./TodoSlice"

function AddItem({ onChange }) {
    const [text, setText] = useState('')
    const dispatch = useDispatch()
    function handleChange(e) {
        setText(e.target.value)
    }
    function handleText() {
        if (text && text.length > 0) {

            dispatch(generateTodo({
                id: Math.ceil(Math.random() * 100000),
                text: text,
                done: false
            }))
            setText('')
        }
    }

    return (
        <div style={{ marginLeft: "50px" }}>
            <input type="text" value={text} onChange={handleChange} />
            <button style={{ marginLeft: "10px", backgroundColor: "lightBlue" }} onClick={handleText}>add</button>
        </div>
    )
}
export default AddItem