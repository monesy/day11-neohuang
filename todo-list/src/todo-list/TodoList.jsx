
import TodoItemGenerator from "./TodoItemGenerator"
import TodoItemGroup from "./TodoItemGroup"
import { useSelector } from "react-redux"

function TodoList() {

    const todoList = useSelector((state) => state.todo.todoList)
    console.log(todoList, "todoList")

    return (
        <div >
            <div style={{ marginLeft: "120px" }}>Todo List</div>
            <TodoItemGroup />
            <TodoItemGenerator />
        </div>
    )
}

export default TodoList