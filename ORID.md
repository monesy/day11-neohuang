### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	上午进行了Code Review。今天上午进行了SpringBoot Concept Map的绘制，然后学习了HTML、CSS 和 JavaScript 的基础知识，为网页开发打下基础。下午进行了 React的学习，遇到了有挑战性的 React 实例 “MultipleCounter”，我花费了大量时间去理解其中的内容。

### R (Reflective): Please use one word to express your feelings about today's class.

​	有点难

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	React 中的 “MultipleCounter” 实例对我来说有一定难度，需要额外的努力和时间来理解其实现和功能。主要原因是从后端转到前端，编程方式发生了一点改变，在适应了这种陌生的方式后就舒服多了。

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	增加时间进行动手编码练习和问题解决，提高学习体验、巩固概念理解。
