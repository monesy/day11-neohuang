### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	今天上午进行了Code Review，然后进行了Redux及其Slice相关知识的学习。下午进行了针对上午学习的内容进行了大量的编码训练，最后对前端的测试进行了初步学习与简单尝试。

### R (Reflective): Please use one word to express your feelings about today's class.

​	比昨天轻松一些

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	在大量的编码训练中我逐渐对前端的编码有了一定感觉，在回家完成homework时我花了很多时间对整个作业进行深入的理解。

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	多编写前端代码，适应这种编码风格，提高对代码的理解，巩固概念知识。
